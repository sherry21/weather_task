import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WeatherDetailComponent } from './weather-detail/weather-detail.component';
import { WeatherListComponent } from './weather-list/weather-list.component';
import { WeatherServiceService  } from './weather-service.service';

const routes: Routes = [
  {path: '', component: WeatherListComponent, pathMatch: 'full'},
  {path: 'weatherInfo', component: WeatherDetailComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

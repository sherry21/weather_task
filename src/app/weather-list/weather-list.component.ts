import { Component, OnInit } from '@angular/core';
import { WeatherServiceService  } from '../weather-service.service';

import { Router } from '@angular/router'

@Component({
  selector: 'app-weather-list',
  templateUrl: './weather-list.component.html',
  styleUrls: ['./weather-list.component.css']
})
export class WeatherListComponent implements OnInit {

  public weathers = [];

  constructor(private _weatherService: WeatherServiceService, private router: Router) { }

  ngOnInit() {
    this._weatherService.getWeatherList()
        .subscribe(data => this.weathers = data.list);
        
  }

  onWeatherClicked(weather) {
    this.router.navigateByUrl('weatherInfo');
    this._weatherService.setWeather(weather);
  }




}


import { Component, OnInit } from '@angular/core';
import { WeatherServiceService  } from '../weather-service.service';

@Component({
  selector: 'app-weather-detail',
  templateUrl: './weather-detail.component.html',
  styleUrls: ['./weather-detail.component.css']
})
export class WeatherDetailComponent implements OnInit {

  public weathers : any;
  constructor(private _weatherService: WeatherServiceService) { }

  ngOnInit() {
    this.weathers = this._weatherService.getDetails();
    console.log("hey" + this.weathers);
  }

}

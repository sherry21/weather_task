import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class WeatherServiceService {
  private _url: string="https://samples.openweathermap.org/data/2.5/forecast/hourly?q=London,us&appid=b6907d289e10d714a6e88b30761fae22";

  constructor(private http:HttpClient) { }
  
  weather:any

  getWeatherList(): Observable<any>{
    return this.http.get<any>(this._url);
  }
  setWeather(weather){
    return this.weather = weather;
  }
  getDetails(){
    return this.weather;
  }

}

